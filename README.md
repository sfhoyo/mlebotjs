# README #

###About###
Mlebot is a chat bot for Discord. It runs on a node.js server (through Express) and can be interacted with through standard http. The server is meant to only be accessed locally as it is essentially a UI for the bot admin to interact with. The purpose of this project is get a chance to work with new (to me) technologies, while also creating something of value for my Discord channel.

###Setup###

####NOTE: These instructions may be incorrect, I have yet to try setting this up on another machine so it's possible I missed some steps.####

Install Node.js. I am using v6.9.5, discord.js requires v6.0.0 to work. Make sure npm is also installed (it should come with Node). I used express-generator to create the directory, so you probably should too. In the future I might add an alternative method.

`npm install -g express-generator`

`express -c stylus <directory name>`

`cd <directory name> && npm install`

`npm install discord.js`

Then you should be able to copy my source code into the main directory and let it overwrite all files.

Create a bot account on Discord: https://github.com/reactiflux/discord-irc/wiki/Creating-a-discord-bot-&-getting-a-token

In the directory you created, create a file called 'token' and paste your bot account's login token. 

Now you can start the server and the bot:

`node ./bin/www`

You can access the webpage at: 127.0.0.1:3000