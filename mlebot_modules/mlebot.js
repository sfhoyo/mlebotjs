const Discord = require("discord.js");
var fs = require('fs');
var client;
function initialize() {
    client = new Discord.Client();
    client.on('ready', () => {
        console.log(`Logged in as ${client.user.username}!`);
    });

    client.on('message', msg => {
        if (msg.content === 'ping') {
            msg.reply('Pong!');
        }
    });
    var token;
    //read token from a file (so i don't have to post it publicly)
    fs.readFile('token', 'utf8', function (err, contents) {
        client.login(contents);
    });
}
var mlebot = function(){
    var turnOn = function(){
        initialize();
        console.log("bot has been turned on");
    }
    var turnOff = function(){
        client.destroy();
        console.log("bot has been turned off");
    }
    
    return{
        turnOff: turnOff,
        turnOn: turnOn
    }
    
}();

module.exports = mlebot;