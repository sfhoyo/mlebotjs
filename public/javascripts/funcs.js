$(document).ready(function(){
    $('#botOn').click(function() {
        $.ajax({
            type: 'POST',
            url: '/botOn',
            success: botOn()
        });      
    });
    
    $('#botOff').click(function() {
        $.ajax({
            type: 'POST',
            url: '/botoff',
            success: botOff()
        });
    });
});

function botOn(){
    console.log('Bot has been turned on');
    $('#botOn').attr('hidden', true);
    $('#botOff').removeAttr('hidden');
}
function botOff(){
    console.log('Bot has been turned off');
    $('#botOff').attr('hidden', true);
    $('#botOn').removeAttr('hidden');
}