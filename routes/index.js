var express = require('express');
var mlebot = require('../mlebot_modules/mlebot');
var router = express.Router();

router.get('/', function(req, res){
  res.render('index', {
    title: 'Home'
  });
});

router.get('/about', function(req, res){
  res.render('index', {
    title: 'About'
  });
});

router.get('/contact', function(req, res){
  res.render('index', {
    title: 'Contact'
  });
});

router.post('/boton',function(req,res){
   mlebot.turnOn();
   res.end('success');
});
router.post('/botoff',function(req,res){
    mlebot.turnOff();
    res.end('success');
});
module.exports = router;